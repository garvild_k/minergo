import * as React from 'react';
import { useState, useEffect, useMemo } from 'react';
import { render } from 'react-dom';
import MapGL, {
    Source, Layer, Popup,
    NavigationControl,
    FullscreenControl,
    ScaleControl,
    GeolocateControl
} from 'react-map-gl';
import ControlPanel from './controlPanel';
import Pins from './pins';
import CityInfo from './city-info';
import weather from './weather.json'

const MAPBOX_TOKEN = 'pk.eyJ1IjoicmluZGluZG4iLCJhIjoiY2ttMXZvbHcyMzBmNzJ3cDMydGR2aGh3OCJ9.qKzBG0CYYZhpFLOdzr35_Q'; // Set your mapbox token here

function filterFeaturesByDay(featureCollection, time) {
    const date = new Date(time);
    const year = date.getFullYear();
    const month = date.getMonth();
    const day = date.getDate();
    const features = featureCollection.features.filter(feature => {
        const featureDate = new Date(feature.properties.time);
        return (
            featureDate.getFullYear() === year &&
            featureDate.getMonth() === month &&
            featureDate.getDate() === day
        );
    });
    return { type: 'FeatureCollection', features };
}

const geolocateStyle = {
    top: 0,
    left: 0,
    padding: '10px'
};

const fullscreenControlStyle = {
    top: 36,
    left: 0,
    padding: '10px'
};

const navStyle = {
    top: 72,
    left: 0,
    padding: '10px'
};

const scaleControlStyle = {
    bottom: 36,
    left: 0,
    padding: '10px'
};

export default function App() {
    const [viewport, setViewport] = useState({
        latitude: 59.939099,
        longitude: 30.315877,
        zoom: 9,
        bearing: 0,
        pitch: 0
    });
    const [allDays, useAllDays] = useState(true);
    const [timeRange, setTimeRange] = useState([0, 0]);
    const [selectedTime, selectTime] = useState(0);
    const [earthquakes, setEarthQuakes] = useState(null);
    const [popupInfo, setPopupInfo] = useState(null);


    useEffect(() => {
        fetch('https://minergo.vercel.app/api/hello')
            .then(resp => resp.json())
            .then(json => {
                const features = json.features;
                const endTime = features[0].properties.time;
                const startTime = features[features.length - 1].properties.time;

                setTimeRange([startTime, endTime]);
                setEarthQuakes(json);
                selectTime(endTime);
            });
    }, []);

    const data = useMemo(() => {
        return allDays ? earthquakes : filterFeaturesByDay(earthquakes, selectedTime);
    }, [earthquakes, allDays, selectedTime]);


    return (
        <>
            <MapGL
                {...viewport}
                width="100%"
                height="100vh"
                mapStyle="mapbox://styles/mapbox/dark-v9"
                onViewportChange={setViewport}
                mapboxApiAccessToken={MAPBOX_TOKEN}
            >
                <Pins data={data} onClick={setPopupInfo} />

                {popupInfo && (
                    <Popup
                        tipSize={5}
                        anchor="top"
                        longitude={popupInfo.geometry.coordinates[1]}
                        latitude={popupInfo.geometry.coordinates[0]}
                        closeOnClick={false}
                        onClose={setPopupInfo}
                    >
                        <CityInfo info={popupInfo} />
                    </Popup>
                )}

                <GeolocateControl style={geolocateStyle} />
                <FullscreenControl style={fullscreenControlStyle} />
                <NavigationControl style={navStyle} />
                <ScaleControl style={scaleControlStyle} />
            </MapGL>
            <ControlPanel
                startTime={timeRange[0]}
                endTime={timeRange[1]}
                selectedTime={selectedTime}
                allDays={allDays}
                onChangeTime={selectTime}
                onChangeAllDays={useAllDays}
            />
        </>
    );
}

export function renderToDom(container) {
    render(<App />, container);
}